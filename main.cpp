/***************************************************************************
 *   Copyright (C) 2018 Jan Fostier (jan.fostier@ugent.be)                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <vector>
#include <cstring>
#include "arrow/io/file.h"
#include "parquet/stream_reader.h"


const std::vector<char> representation ({ '-', 'A', 'C', 'M', 'G', 'R', 'S', 'V', 'T', 'W', 'Y', 'H', 'K', 'D', 'B', 'N' });
const int longSize = 8;
const int blscountsize = 2;

void printMotifAndGroupAndGetBlsVectorSize(int motifLength, char *motifdata, std::ostream &out, int &blsvectorsize) {
    for (int i = 0; i < ((motifLength  + 1)>> 1); i++) {
        std::cout << representation[(motifdata[i] >> 4) & 0xf];
        if(i*2+1 < motifLength)
            std::cout << representation[motifdata[i] & 0xf];
    }
    out << "\t";
    for (int i = 0; i < ((motifLength + 1) >> 1); i++) {
        std::cout << representation[(motifdata[((motifLength  + 1)>> 1)+i] >> 4) & 0xf];
        if(i*2+1 < motifLength)
            std::cout << representation[motifdata[((motifLength  + 1)>> 1)+i] & 0xf];
    }
    blsvectorsize = motifdata[2 *((motifLength + 1) >> 1)];
}

void printBlsVectorCounts(int blsvectorsize, unsigned char *blsvectorcounts, std::ostream &out) {
    int n = 1;

    unsigned short count = 0;
    for (int i = 0, j = 0; i < blsvectorsize; i++, j+=blscountsize) {
        // little endian if true
        if(*(char *)&n == 1) {
            count = blsvectorcounts[j] | (blsvectorcounts[j + 1] << 8);
        } else {
            count = blsvectorcounts[j + 1] | (blsvectorcounts[j] << 8);
        }
//        count = blsvectorcounts[j];
        out << "\t" << count;
    }
}

void deserializeMotifs(std::istream &in, std::ostream &out) {
    int blsvectorsize = 0;
    int motifLength = in.get();
    char *motifAndGroup = new char[longSize*2];
    while (in) {
        int data_length = 2 *((motifLength + 1) >> 1) + 1;
        std::memset(motifAndGroup, 0, longSize * 2);
        in.read(motifAndGroup, data_length);
        if(in) {
            printMotifAndGroupAndGetBlsVectorSize(motifLength, motifAndGroup, out, blsvectorsize);
            out << "\t" << blsvectorsize;
            motifLength = in.get();
        }
        out << "\n";

    }
    delete motifAndGroup;
}

void deserializeCountedMotifs(std::istream &in, std::ostream &out) {
    int blsvectorsize = 0;
    int motifLength = in.get();
    char *motifAndGroup = new char[longSize*2];
    int currentblsvectorsize = 0;
    unsigned char *blsVector = NULL;
    while (in) {
        int data_length = 2 *((motifLength + 1) >> 1) + 1;
//        std::cerr << "data length " << data_length << std::endl;
        std::memset(motifAndGroup, 0, longSize * 2);
        in.read(motifAndGroup, data_length);
        if(in) {
            printMotifAndGroupAndGetBlsVectorSize(motifLength, motifAndGroup, out, blsvectorsize);
            if (currentblsvectorsize < blsvectorsize) {
                blsVector = (unsigned char *)realloc(blsVector, blsvectorsize*blscountsize);
                currentblsvectorsize = blsvectorsize;
            }
            in.read((char *)blsVector, blsvectorsize*blscountsize);
            printBlsVectorCounts(blsvectorsize, blsVector, out);
            motifLength = in.get();
        }
        out << "\n";

    }
    delete blsVector;
    delete motifAndGroup;
}

void readParquetFile(char *input, std::ostream &out) {
    std::shared_ptr<arrow::io::ReadableFile> infile;

    PARQUET_ASSIGN_OR_THROW(
       infile,
       arrow::io::ReadableFile::Open(input));

// todo read number of strings as well, so we ccan read the output parquet format as well
    int ints = 0;
    int strings = 0;
    int i = 0;
    const parquet::SchemaDescriptor* schema = parquet::ParquetFileReader::Open(infile)->metadata()->schema();
    while(i < schema->num_columns() && schema->Column(i)->converted_type() == parquet::ConvertedType::UTF8) {
        strings++;
        i++;
    }
    while(i < schema->num_columns() && schema->Column(i)->converted_type() == parquet::ConvertedType::INT_32) {
        ints++;
        i++;
    }


    parquet::StreamReader os{parquet::ParquetFileReader::Open(infile)};
    std::string motif;
    int32_t bls_count;

    while ( !os.eof() ) {
        os >> motif;
        out << motif;
        for (int i = 1; i < strings; i++) {
            os >> motif;
            out << "\t" << motif;
        }
        for (int i = 0; i < ints; i++) {
            os >> bls_count;
            out << "\t" << bls_count;
        }
        out << "\n";
        os >> parquet::EndRow;
    }

}

struct configuration {
    char *input;
    bool parquet = false;
    bool counted = false;
    int blsthresholds = 0;
};

configuration *parseConfiguration(int argc, char* argv[]) {
    configuration *c = new configuration();
    int i = 1;
    bool exit = false;
    bool printhelp = false;
    while (i < argc && !exit) {
        if(strcmp(argv[i], "-h") == 0) {
            printhelp = true;
            exit = true;
        } else if (strcmp(argv[i], "--parquet") == 0) {
            c->parquet = true;
            i++;
            if(i < argc)
                c->input = argv[i];
            else {
                exit = true;
                std::cerr << "missing input file for parquet" << std::endl;
            }
        } else if (strcmp(argv[i], "--counted") == 0 ) {
            c->counted = true;
        } else {
            std::cerr << "Unknown option " << argv[i] << std::endl;
            exit = true;
        }
        i++;
    }

    if (printhelp) {
        std::cerr << "usage: motifdeserializer [options]" << std::endl;
        std::cerr << "  options:" << std::endl;
        std::cerr << "\t--counted:\tbinary counted format." << std::endl;
        std::cerr << "\t--parquet <file>:\tread parquet file" << std::endl;
    }
    if(c->parquet && strcmp(c->input, "-") == 0) {
        std::cerr << "Parquet input requires a file." << std::endl;
        return NULL;
    }
    // if(c->parquet && c->blsthresholds == 0) {
    //     std::cerr << "Number of BLS thresholds required for parquet input." << std::endl;
    //     return NULL;
    // }
    if (exit) {
        return NULL;
    }
    return c;
}

int main(int argc, char* argv[])
{
    configuration *c = parseConfiguration(argc, argv);
    if(c == NULL) {
        return EXIT_FAILURE;
    } else {
        if(c->parquet) {
            readParquetFile(c->input, std::cout);
        } else {
            std::cerr << "reading from stdin" << std::endl;
            stdin = freopen(NULL, "rb", stdin);
            if (c->counted)
                deserializeMotifs(std::cin, std::cout);
            else
                deserializeCountedMotifs(std::cin, std::cout);
            fclose(stdin);
        }
        return EXIT_SUCCESS;
    }
}
