# install

## install arrow/parquet

```bash
sudo apt-get update
sudo apt-get install -y -V ca-certificates lsb-release wget
wget https://apache.jfrog.io/artifactory/arrow/$(lsb_release --id --short | tr 'A-Z' 'a-z')/apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
sudo apt-get install -y -V ./apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
sudo apt-get update
sudo apt-get install -y -V libarrow-dev libarrow-glib-dev libparquet-dev libparquet-glib-dev
# or similar; based on https://arrow.apache.org/install/
```

## build binary
```bash
mkdir build
cd build
cmake ../
make
```
